import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {ContactComponent} from './contact/contact.component';

const routes = [
    {
      path: '',
      component: AppComponent,
    },
    {
      path: 'contact',
      component: ContactComponent
    },
    {
      path: 'blog',
      loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule)
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule {

  constructor() {
  }
}
