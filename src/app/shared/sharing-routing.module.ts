import {BlogComponent} from './blog/blog.component';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

const routes = [{
  path: '', component: BlogComponent,
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
